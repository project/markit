<?php

namespace Drupal\markit;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\markit\Entity\MarkItInterface;
use Drupal\markit\Entity\MarkItType;
use Drupal\Core\Session\AccountInterface;
use Drupal\markit\Exceptions\MarkItNotAllowedEntityAction;

/**
 * Class MarkItManager
 *
 * @package Drupal\markit
 */
class MarkItManager {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager.
   */
  protected $entityTypeManager;

  /**
   * The configuration service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configurationFactory;

  /**
   * MarkItAddonManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entityTypeManager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection service.
   * @param \Drupal\Core\Config\ConfigFactory $configurationFactory
   *   The config_factory service.
   */
  public function __construct(EntityTypeManager $entityTypeManager, Connection $connection, ConfigFactory $configurationFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->connection = $connection;
    $this->configurationFactory = $configurationFactory;
  }

  /**
   * Gets the markit_type entities.
   *
   * @return array
   *   Returns an array with the ids as keys and the labels as values.
   */
  public function getMarkItTypes() {
    $bundles = MarkItType::loadMultiple();
    $options = [];
    foreach ($bundles as $bundle => $information) {
      $options[$bundle] = $information->label();
    }
    return $options;
  }

  /**
   * Do mark.
   *
   * @param string $markit_type
   *   The id the of the MarkType entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Target entity.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   (optional) User account.
   * @param numeric $score
   *   (optional) Score related to marking.
   * @param \Drupal\markit\Entity\MarkItInterface|null $markit
   *   (optional) Pass a variable to obtain the created MarkIt entity.
   *
   * @return array
   *   Returns an array keyed by the $markit_type with the number of marks.
   *
   * @throws \Drupal\markit\Exceptions\MarkItNotAllowedEntityAction
   */
  public function doMark($markit_type, EntityInterface $entity, AccountInterface $account = NULL, $score = NULL, MarkItInterface &$markit = NULL) {
    if ($account === NULL) {
      $account = \Drupal::currentUser();
    }

    $marked = $this->isMarked($markit_type, $entity, $account);
    if (!$marked) {
      $entity_type = $entity->getEntityType()->id();
      $values = [
        'type' => $markit_type,
        'uid' => $account->id(),
        'target_entity_type' => $entity_type,
        'target_entity_id' => $entity->id(),
        'score' => $score,
      ];

      // Create new markit entity.
      $markit = $this->entityTypeManager->getStorage('markit')->create($values);
      $markit->save();

      // Reset entity cache.
      $this->entityTypeManager
        ->getViewBuilder($entity->getEntityTypeId())
        ->resetCache([$entity]);
    }

    return [
      $markit_type => $entity->get('markit')->value[$markit_type] ?? 0,
    ];
  }

  /**
   * Checks if the entity is already at the desired state.
   *
   * @param string $markit_type_id
   *   The id of the MarkitType entity to check.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   * @param \Drupal\Core\Session\AccountInterface|null $user
   *   The account of the user to check.
   *
   * @return bool
   *   Returns TRUE if the entity has already been marked with this type. FALSE
   *   will be returned otherwise.
   */
  public function isMarked($markit_type_id, EntityInterface $entity, AccountInterface $user = NULL) {
    if ($user === NULL) {
      $user = \Drupal::currentUser();
    }

    if (!$this->markItTypeValidForEntity($markit_type_id, $entity->getEntityTypeId(), $entity->bundle())) {
      throw new MarkItNotAllowedEntityAction("Entity {$entity->getEntityTypeId()}:{$entity->bundle()}:{$entity->id()} not allowed for (un)marking with {$markit_type_id}.");
    }

    $storage = $this->entityTypeManager->getStorage('markit');
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $markit_type_id)
      ->condition('target_entity_type', $entity->getEntityTypeId())
      ->condition('target_entity_id', $entity->id())
      ->condition('uid', $user->id());
    $markings = $query->execute();
    if (!empty($markings)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Check if a markit_type is enabled for an entity.
   *
   * @param string $markit_type_id
   *   The id of the MarkitType entity.
   * @param string $entity_type
   *   The entity type to check for validity.
   * @param string $bundle
   *   The entity type's bundle to check for validity.
   *
   * @return bool
   *   Returns TRUE if the action is enabled and should proceed. FALSE
   *   otherwise.
   */
  public function markItTypeValidForEntity($markit_type_id, $entity_type, $bundle) {
    $cid = implode(':', [
      $entity_type,
      $bundle
    ]);
    $configuration = $this->configurationFactory->get('markit.settings');

    $target_entities = $configuration->get('target_entities');
    if (!isset($target_entities[$cid])) {
      // If the admin hasn't allowed this entity's bundle, it is invalid.
      return FALSE;
    }

    $target_entities_actions = $configuration->get('target_entities_actions');
    if (empty($target_entities_actions[$cid][$markit_type_id])) {
      // If the markit_type is not enabled, it is invalid.
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Do unmark.
   *
   * @param string $markit_type
   *   The id of the MarkType entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Target entity.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   (optional) User account.
   * @param \Drupal\markit\Entity\MarkItInterface[]|null $markit
   *   (optional) Pass a variable to obtain an array with the markit entities to
   *   be deleted.
   *
   * @return int[]
   *   The current count of the $markit_type keyed by the $markit_type.
   */
  public function doUnmark($markit_type, EntityInterface $entity, AccountInterface $account = NULL, array &$markit = NULL) {
    if ($account === NULL) {
      $account = \Drupal::currentUser();
    }

    if ($this->isMarked($markit_type, $entity, $account)) {
      $entity_type = $entity->getEntityType()->id();
      $storage = \Drupal::entityTypeManager()->getStorage('markit');
      $query = $storage->getQuery()
        ->accessCheck(TRUE)
        ->condition('type', $markit_type)
        ->condition('target_entity_type', $entity_type)
        ->condition('target_entity_id', $entity->id())
        ->condition('uid', $account->id());
      $marks = $query->execute();

      $markit = $storage->loadMultiple($marks);
      $storage->delete($markit);

      // Reset entity cache.
      $this->entityTypeManager
        ->getViewBuilder($entity->getEntityTypeId())
        ->resetCache([$entity]);
    }

    return [
      $markit_type => $entity->get('markit')->value[$markit_type] ?? 0,
    ];
  }

  /**
   * Returns how many markings the entity has.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to parse.
   * @param string $markit_type_id
   *   The markit_type to filter the markings.
   *
   * @return array
   *   Returns the number of markings the entity has.
   */
  public function getCountsOfEntity(EntityInterface $entity, $markit_type_id = NULL) {
    $query = $this->connection->select('markit', 'm')
      ->condition('m.target_entity_type', $entity->getEntityTypeId())
      ->condition('m.target_entity_id', $entity->id());
    $query->addField('m', 'type');
    $query->addExpression('COUNT(*)');
    $query->groupBy('m.type');
    if ($markit_type_id) {
      $query->condition('type', $markit_type_id);
    }
    $query->innerJoin('users_field_data', 'u', 'u.uid = m.uid AND u.status = 1');
    return $query->execute()->fetchAllKeyed();
  }

  /**
   * Get the UIDs of the users who marked the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to process.
   * @param string $markit_type_id
   *   The markit_type to filter the markings.
   *
   * @return array
   *   Returns an array with the IDs of the users who have marked the entity. If
   *   $markit_type is not specified an array of arrays will be returned,
   *   grouped by the available markit_type entities.
   */
  public function getUsersMarkEntity(EntityInterface $entity, $markit_type_id = NULL) {
    $results = [];
    if (!$markit_type_id) {
      $markit_types = MarkItType::loadMultiple();
      foreach ($markit_types as $type) {
        $markit_type_id = $type->id();
        $results[$markit_type_id] = $this->getUsersMarkEntity($entity, $markit_type_id);
      }
      return $results;
    }

    $query = $this->connection->select('markit', 'm')
      ->fields('m', ['uid'])
      ->condition('m.target_entity_type', $entity->getEntityTypeId())
      ->condition('m.target_entity_id', $entity->id())
      ->condition('m.type', $markit_type_id)
      ->orderBy('m.created', 'ASC');
    $query->innerJoin('users_field_data', 'u', 'u.uid = m.uid AND u.status = 1');
    $results[$markit_type_id] = $query->execute()->fetchCol();

    return $results[$markit_type_id];
  }

  /**
   * Get the average score of the users who marked the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to process.
   * @param null $markit_type_id
   *   The markit_type to filter the markings.
   *
   * @returns int|null
   *   Returns the average percentage of the score. NULL if no records exist.
   */
  public function getAveragePercentageScore(EntityInterface $entity, $markit_type_id = NULL) {
    $query = $this->connection->select('markit', 'm');
    $query->addExpression('SUM(m.score)', 'score');
    $query->addExpression('COUNT(*)', 'counter');
    $query->condition('m.target_entity_type', $entity->getEntityTypeId())
      ->condition('m.target_entity_id', $entity->id())
      ->condition('m.type', $markit_type_id);
    $result = $query->execute()->fetchObject();
    if ($result->counter > 0) {
      return $result->score / $result->counter;
    }
    return NULL;
  }

  /**
   * Get the current user's score after having marked an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to process.
   * @param null $markit_type_id
   *   The markit_type to filter the markings.
   *
   * @returns int|null
   *   Returns the current user's score. NULL if no records exist.
   */
  public function getCurrentUserPercentageScore(EntityInterface $entity, $markit_type_id) {
    $query = $this->connection->select('markit', 'm')
      ->fields('m', ['score'])
      ->condition('m.target_entity_type', $entity->getEntityTypeId())
      ->condition('m.target_entity_id', $entity->id())
      ->condition('m.type', $markit_type_id);
    return $query->execute()->fetchField()->score;
  }

}
