<?php

namespace Drupal\markit\Fields;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;

/**
 * Class MarkItItemList.
 */
class MarkItItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  public function computeValue() {
    /** @var \Drupal\markit\MarkItManager $service */
    $service = \Drupal::service('markit.manager');
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->getEntity();
    $list[] = $this->createItem(0, [
      'value' => $service->getCountsOfEntity($entity),
    ]);
    $this->list = $list;
  }

}
