<?php

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Adds a score property to the markit entity.
 */
function markit_post_update_1000_add_score_property(&$sandbox) {
  $definition_update_manager = \Drupal::entityDefinitionUpdateManager();

  // Process for the point entity type.
  $entity_type = $definition_update_manager->getEntityType('markit');
  $storage_definition = BaseFieldDefinition::create('decimal')
    ->setLabel(t('Score'))
    ->setDescription(t('Some types may need a score to be set'))
    ->setSettings([
      'unsigned', TRUE,
      'min' => 0,
      'max' => 100,
    ]);
  $definition_update_manager->installFieldStorageDefinition('score', $entity_type->id(), 'markit', $storage_definition);
}
