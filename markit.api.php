<?php

/**
 * @file
 * Contains the hook implementations of this module.
 */

/**
 * Alters the response of the MarkItController::mark().
 *
 * @param array $response
 *   The response to alter.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity marked.
 * @param \Drupal\markit\Entity\MarkItInterface $markit
 *   The markit entity. May be NULL if this is the not the first time the route
 *   gets called.
 */
function hook_markit_MARKIT_TYPE_marked_alter(array &$response, \Drupal\Core\Entity\EntityInterface $entity, \Drupal\markit\Entity\MarkItInterface $markit = NULL) {
  $response['marked'] = TRUE;
}

/**
 * Alters the response of the MarkItController::unmark().
 *
 * @param array $response
 *   The response to alter.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity marked.
 * @param \Drupal\markit\Entity\MarkItInterface[] $markits
 *   An array containing the matching markit entities; most possibly just one.
 *   May be NULL if this is not the first time the route gets called.
 */
function hook_markit_MARKIT_TYPE_unmarked_alter(array &$response, \Drupal\Core\Entity\EntityInterface $entity, array $markits = NULL) {
  $response['unmarked'] = FALSE;
}

/**
 * Access check for an action.
 *
 * There may be cases where a marked state should be dependant on another. This
 * scenario is not configurable from this module alone. You could though, using
 * the hook below, get the entity, its markit_type, its action (mark/unmark)
 * etc. and do your own checks using the "markit.manager" service provided by
 * this module.
 *
 * Warning: This hook is invoked only if the access checks of this module pass!
 *
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The account to check.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The the entity to check.
 * @param string $markit_type_id
 *   The id of the markit_type entity.
 * @param string $action
 *   The action on the markit_type. For the allowed values, check the constants
 *   of the \Drupal\markit\Controller\MarkItController class.
 *
 * @return false|void
 *   Return FALSE if the module shouldn't allow access. Any other value
 *   will keep looking for a module to return FALSE. If no modules return FALSE,
 *   the action will be allowed.
 */
function hook_markit_access_check(\Drupal\Core\Session\AccountInterface $account, \Drupal\Core\Entity\EntityInterface $entity, $markit_type_id, $action) {
  /** @var \Drupal\markit\MarkItManager $service */
  $service = \Drupal::service('markit.manager');
  if ($markit_type_id === 'commented') {
    if (!$service->isMarked('viewed', $entity, $account)) {
      // Don't allow an entity to be set as "commented" if it hasn't been
      // previously marked as "viewed".
      return FALSE;
    }
  }
}
