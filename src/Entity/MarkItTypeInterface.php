<?php

namespace Drupal\markit\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining MarkIt type entities.
 */
interface MarkItTypeInterface extends ConfigEntityInterface {

  /**
   * Get the prefix of the type.
   *
   * @return string
   */
  public function getPrefix();

  /**
   * Get the suffix of the type.
   *
   * @return string
   */
  public function getSuffix();

}
