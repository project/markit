<?php

namespace Drupal\markit\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining MarkIt entities.
 *
 * @ingroup markit
 */
interface MarkItInterface extends ContentEntityInterface, EntityOwnerInterface {

  const MARKED = 1;
  const UNMARKED = 0;

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the MarkIt creation timestamp.
   *
   * @return int
   *   Creation timestamp of the MarkIt.
   */
  public function getCreatedTime();

  /**
   * Sets the MarkIt creation timestamp.
   *
   * @param int $timestamp
   *   The MarkIt creation timestamp.
   *
   * @return \Drupal\markit\Entity\MarkItInterface
   *   The called MarkIt entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Likeit target entity id.
   *
   * @return int
   *   Target entity id.
   */
  public function getTargetEntityId();

  /**
   * Sets the Likeit target entity id.
   *
   * @param int $target_entity_id
   *   Target entity id.
   *
   * @return \Drupal\markit\Entity\MarkItInterface
   *   The called Likeit entity.
   */
  public function setTargetEntityId($target_entity_id);

  /**
   * Gets the Likeit target entity type.
   *
   * @return string
   *   Target entity type.
   */
  public function getTargetEntityType();

  /**
   * Sets the MarkIt target entity type.
   *
   * @param string $target_entity_type
   *   Target entity type.
   *
   * @return \Drupal\markit\Entity\MarkItInterface
   *   The called MarkIt entity.
   */
  public function setTargetEntityType($target_entity_type);

  /**
   * Gets the MarkIt target entity.
   *
   * @return \Drupal\Core\Entity\Entity
   *   Target entity.
   */
  public function getTargetEntity();

  /**
   * Gets the score (if any).
   *
   * @return int
   */
  public function getScore();

  /**
   * Sets the score (if available).
   *
   * @param int $score
   *   The score to be set.
   *
   * @return \Drupal\markit\Entity\MarkItInterface
   */
  public function setScore($score);

}
