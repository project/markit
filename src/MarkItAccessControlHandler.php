<?php

namespace Drupal\markit;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\node\Plugin\views\filter\Access;

/**
 * Access controller for the MarkIt entity.
 *
 * @see \Drupal\markit\Entity\MarkIt.
 */
class MarkItAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\markit\Entity\MarkItInterface $entity */

    switch ($operation) {

      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view markit entities')
          ->orIf(AccessResult::allowedIfHasPermission($account, "view {$entity->bundle()} markit entities"));

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'unmark markit entities')
          ->orIf(AccessResult::allowedIfHasPermission($account, "unmark {$entity->bundle()} markit entities"));
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'mark markit entities')
      ->orIf(AccessResult::allowedIfHasPermission($account, "mark {$entity_bundle} markit entities"));
  }

}
