<?php

namespace Drupal\markit\Entity;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\views\EntityViewsData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Views data for MarkIt entities.
 */
class MarkItViewsData extends EntityViewsData {

  /**
   * The config.factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    /** @var \Drupal\markit\Entity\MarkItViewsData $class */
    $class = parent::createInstance($container, $entity_type);
    $class->setConfigFactory($container->get('config.factory'));
    return $class;
  }

  /**
   * Sets the config.factory service.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   A config factory service instance.
   */
  protected function setConfigFactory(ConfigFactory $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Retrieve the entity types & bundles for which MarkIt is enabled.
    $configuration = $this->configFactory->get('markit.settings');
    $target_entity_bundles = $configuration->get('target_entities') ?? [];
    $target_entity_types = [];
    foreach ($target_entity_bundles as $target_entity_bundle) {
      [$entity_type] = explode(':', $target_entity_bundle);
      $target_entity_types[$entity_type] = $entity_type;
    }

    // Loop over the entity_types and build proper relationships.
    foreach ($target_entity_types as $target_entity_type) {
      /** @var \Drupal\Core\Entity\ContentEntityType $information */
      $information = $this->entityTypeManager->getDefinition($target_entity_type);
      $base_table = $information->getBaseTable();
      $keys = $information->getKeys();

      // Not tested yet! Uncomment it and test it if you need it.
      // $data['markit']['target_entity__' . $target_entity_type] = [
      //   'title' => $this->t('Target Entity - @type', [
      //     '@type' => $target_entity_type,
      //   ]),
      //   'help' => $this->t('Relate to the referenced @type entity.', [
      //     '@type' => $target_entity_type,
      //   ]),
      //   'relationship' => [
      //     'id' => 'standard',
      //     'base field' => 'target_entity_id',
      //     'field' => $keys['id'],
      //   ],
      // ];.
      $data["{$base_table}_field_data"]['markit'] = [
        'title' => $this->t('MarkIt'),
        'help' => $this->t('Relate MarkIt records to the entity. This relationship will create one record for each MarkIt record in database. Use proper filters or aggregation to limit the results.'),
        'relationship' => [
          'id' => 'standard',
          'base' => $this->entityType->getBaseTable() ?: $this->entityType->id(),
          'base field' => 'target_entity_id',
          'field' => $keys['id'],
          'extra' => [
            [
              'field' => 'target_entity_type',
              'value' => $target_entity_type,
            ],
          ],
        ],
      ];

      $data["{$base_table}_field_data"]['markit_self_reference'] = [
        'title' => $this->t('Markit Self Reference'),
        'help' => $this->t('Relate MarkIt records to the entity using as a target a specific markit type.'),
        'relationship' => [
          'id' => 'markit_self_reference',
          'base' => $this->entityType->getBaseTable() ?: $this->entityType->id(),
          'base field' => 'target_entity_id',
          'field' => $keys['id'],
          'extra' => [
            [
              'field' => 'target_entity_type',
              'value' => $target_entity_type,
            ],
          ],
        ],
      ];

      $data['markit'][$target_entity_type . '__target_entity_id'] = [
        'title' => $this->t('Join on @entity_type table', ['@entity_type' => ucfirst($target_entity_type)]),
        'help' => $this->t('Relate MarkItRecords to the entity referenced'),
        'relationship' => [
          'id' => 'standard',
          'label' => ucfirst($target_entity_type),
          'base' => $target_entity_type . '_field_data',
          'base field' => $keys['id'],
          'field' => 'target_entity_id',
          'extra' => [
            [
              'left_field' => 'target_entity_type',
              'value' => $target_entity_type,
            ],
          ],
        ],
      ];
    }

    $data["users_field_data"]['markit'] = [
      'title' => $this->t('MarkIt'),
      'help' => $this->t('Relate MarkIt records to the user. This relationship will create one record for each MarkIt record in database. Use proper filters or aggregation to limit the results.'),
      'relationship' => [
        'label' => $this->t('Markit'),
        'id' => 'standard',
        'base' => $this->entityType->getBaseTable() ?: $this->entityType->id(),
        'base field' => 'uid',
        'field' => 'uid',
      ],
    ];

    return $data;
  }

}
