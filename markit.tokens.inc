<?php

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function markit_token_info() {
  $tokens = [];
  /** @var \Drupal\markit\MarkItManager $service */
  $service = \Drupal::service('markit.manager');
  $markit_types = $service->getMarkItTypes();
  $entity_types = array_keys(\Drupal::entityTypeManager()->getDefinitions());
  /** @var \Drupal\Core\Entity\EntityTypeBundleInfo $bundle_service */
  $bundle_service = \Drupal::service('entity_type.bundle.info');
  foreach ($entity_types as $entity_type) {
    foreach ($markit_types as $markit_type_id => $markit_type_label) {
      $bundles = array_keys($bundle_service->getBundleInfo($entity_type));
      foreach ($bundles as $bundle) {
        if ($service->markItTypeValidForEntity($markit_type_id, $entity_type, $bundle)) {
          $tokens[$entity_type]["markit:{$markit_type_id}:average:score"] = [
            'name' => t('@label average score', ['@label' => $markit_type_label]),
            'description' => t('Average percentage score of the @markit marking', ['@markit' => $markit_type_label]),
          ];
          $tokens[$entity_type]["markit:{$markit_type_id}:average:score:?"] = [
            'name' => t('@label average score (customized)', ['@label' => $markit_type_label]),
            'scription' => t('Average score of the @markit marking with a maximum of the number set in place of ?', ['@markit' => $markit_type_label]),
          ];
          $tokens[$entity_type]["markit:{$markit_type_id}:current_user:score"] = [
            'name' => t('@label current user\'s score', ['@label' => $markit_type_label]),
            'description' => t('Current user\'s score of the @markit marking', ['@markit' => $markit_type_label]),
          ];
          $tokens[$entity_type]["markit:{$markit_type_id}:current_user:score:?"] = [
            'name' => t('@label current user\'s score', ['@label' => $markit_type_label]),
            'description' => t('Current user\'s score of the @markit marking with a maximum of the number set in place of ?', ['@markit' => $markit_type_label]),
          ];
        }
      }
    }
  }
  return array(
    'tokens' => $tokens,
  );
}

/**
 * Implements hook_tokens().
 */
function markit_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = array();
  $is_entity_type = \Drupal::entityTypeManager()->getDefinition($type, FALSE);
  if ($is_entity_type && !empty($data[$type])) {
    /** @var \Drupal\markit\MarkItManager $service */
    $service = \Drupal::service('markit.manager');
    foreach ($tokens as $name => $original) {
      if (preg_match('/^markit:(.*):average:score$/', $name, $matches)) {
        /** @var \Drupal\Core\Entity\EntityInterface $entity */
        $entity = $data[$type];
        $markit_type_id = $matches[1];
        if ($service->markItTypeValidForEntity($markit_type_id, $entity->getEntityTypeId(), $entity->bundle())) {
          $score = $service->getAveragePercentageScore($entity, $markit_type_id) ?? 0;
          $replacements[$original] = $score;
        }
      }
      elseif (preg_match('/^markit:(.*):average:score:(.*)$/', $name, $matches)) {
        /** @var \Drupal\Core\Entity\EntityInterface $entity */
        $entity = $data[$type];
        $markit_type_id = $matches[1];
        $max = $matches[2];
        if ($service->markItTypeValidForEntity($markit_type_id, $entity->getEntityTypeId(), $entity->bundle())) {
          $score = $service->getAveragePercentageScore($entity, $markit_type_id);
          if (isset($score) && is_numeric($max) && $max !== '0') {
            $replacements[$original] = round($score * $max / 100, 1);
            if ($replacements[$original] == (int) $replacements[$original]) {
              $replacements[$original] = (int) $replacements[$original];
            }
          }
          else {
            $replacements[$original] = '-';
          }
        }
      }
      elseif (preg_match('/^markit:(.*):current_user:score$/', $name, $matches)) {
        /** @var \Drupal\Core\Entity\EntityInterface $entity */
        $entity = $data[$type];
        $markit_type_id = $matches[1];
        if ($service->markItTypeValidForEntity($markit_type_id, $entity->getEntityTypeId(), $entity->bundle())) {
          $score = $service->getCurrentUserPercentageScore($entity, $markit_type_id);
          $replacements[$original] = $score ?? 0;
        }
      }
      elseif (preg_match('/^markit:(.*):current_user:score:(.*)$/', $name, $matches)) {
        /** @var \Drupal\Core\Entity\EntityInterface $entity */
        $entity = $data[$type];
        $markit_type_id = $matches[1];
        $max = $matches[2];
        if ($service->markItTypeValidForEntity($markit_type_id, $entity->getEntityTypeId(), $entity->bundle())) {
          $score = $service->getCurrentUserPercentageScore($entity, $markit_type_id);
          if (isset($score) && is_numeric($max) && $max !== '0') {
            $replacements[$original] = $score * $max / 100;
            if ($replacements[$original] == (int) $replacements[$original]) {
              $replacements[$original] = (int) $replacements[$original];
            }
          }
          else {
            $replacements[$original] = '-';
          }
        }
      }
    }
  }
  return $replacements;
}

