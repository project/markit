<?php

namespace Drupal\markit;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\markit\Entity\MarkIt;
use Drupal\markit\Entity\MarkItType;


/**
 * Provides dynamic permissions for MarkIt of different types.
 *
 * @ingroup markit
 *
 */
class MarkItPermissions{

  use StringTranslationTrait;

  /**
   * Returns an array of node type permissions.
   *
   * @return array
   *   The MarkIt by bundle permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function generatePermissions() {
    $perms = [];
    /** @var \Drupal\markit\Entity\MarkItType[] $types */
    $types = MarkItType::loadMultiple();
    foreach ($types as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns a list of permissions for a given type.
   *
   * @param \Drupal\markit\Entity\MarkItType $type
   *   The MarkIt type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(MarkItType $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "mark markit entities:{$type_id}" => [
        'title' => $this->t('Mark entities using %type_name', $type_params),
      ],
      "unmark markit entities:{$type_id}" => [
        'title' => $this->t('Unmark entities using %type_name', $type_params),
      ],
    ];
  }

}
