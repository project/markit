<?php

namespace Drupal\markit\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the MarkIt type entity.
 *
 * @ConfigEntityType(
 *   id = "markit_type",
 *   label = @Translation("MarkIt type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\markit\MarkItTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\markit\Form\MarkItTypeForm",
 *       "edit" = "Drupal\markit\Form\MarkItTypeForm",
 *       "delete" = "Drupal\markit\Form\MarkItTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\markit\MarkItTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "markit_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "markit",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/markit_type/{markit_type}",
 *     "add-form" = "/admin/structure/markit_type/add",
 *     "edit-form" = "/admin/structure/markit_type/{markit_type}/edit",
 *     "delete-form" = "/admin/structure/markit_type/{markit_type}/delete",
 *     "collection" = "/admin/structure/markit_type"
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "prefix",
 *     "suffix"
 *   }
 * )
 */
class MarkItType extends ConfigEntityBundleBase implements MarkItTypeInterface {

  /**
   * The MarkIt type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The MarkIt type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The MarkIt type prefix.
   *
   * @var string
   */
  protected $prefix;

  /**
   * The MarkIt type suffix.
   *
   * @var string
   */
  protected $suffix;

  /**
   * {@inheritdoc}
   */
  public function getSuffix() {
    return isset($this->suffix) ? $this->suffix : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrefix() {
    return isset($this->prefix) ? $this->prefix : NULL;
  }

}
