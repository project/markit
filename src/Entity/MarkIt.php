<?php

namespace Drupal\markit\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the MarkIt entity.
 *
 * @ingroup markit
 *
 * @ContentEntityType(
 *   id = "markit",
 *   label = @Translation("MarkIt"),
 *   bundle_label = @Translation("MarkIt"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\markit\MarkItListBuilder",
 *     "views_data" = "Drupal\markit\Entity\MarkItViewsData",
 *   },
 *   base_table = "markit",
 *   translatable = FALSE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer markit entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uid" = "uid",
 *     "uuid" = "uuid",
 *   },
 *   bundle_entity_type = "markit_type",
 *   field_ui_base_route = "entity.markit_type.edit_form"
 * )
 */
class MarkIt extends ContentEntityBase implements MarkItInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function createDuplicate() {
    $duplicate = parent::createDuplicate();
    // Reset the static cache in case we need to migrate the entity to another
    // bundle.
    $duplicate->entityKeys = [];
    return $duplicate;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    // We shouldn't allow the same entity to be marked again for the same user.
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $this->bundle())
      ->condition('uid', $this->getOwnerId())
      ->condition('target_entity_type', $this->getTargetEntityType())
      ->condition('target_entity_id', $this->getTargetEntityId());
    if (!empty($this->id())) {
      $query->condition('id', $this->id(), '!=');
    }
    $conflicts = $query->execute();
    if ($conflicts) {
      throw new \LogicException('Entity may not exist multiple times for the same user, type and target entity.');
    }
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += [
      'name' => sha1(microtime(TRUE)),
      'uid' => \Drupal::currentUser()->id()
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityId() {
    return $this->get('target_entity_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetEntityId($target_entity_id) {
    $this->set('target_entity_id', $target_entity_id);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityType() {
    return $this->get('target_entity_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetEntityType($target_entity_type) {
    $this->set('target_entity_type', $target_entity_type);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntity() {
    return $this->entityTypeManager()
      ->getStorage($this->getTargetEntityType())
      ->load($this->getTargetEntityId());
  }

  /**
   * {@inheritdoc}
   */
  public function getScore() {
    return $this->get('score')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setScore($score) {
    $this->set('score', $score);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /* @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner ID'))
      ->setDescription(t('The ID of the user acting as the entity owner.'))
      ->setSettings([
        'target_type' => 'user',
        'default_value' => 0,
      ]);

    $fields['target_entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Target Entity Type'))
      ->setDescription(t('The target entity type.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ]);

    $fields['target_entity_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Target Entity ID'))
      ->setRequired(TRUE)
      ->setDescription(t('The target entity ID.'));

    $fields['target_entity'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Target Entity'))
      ->setDescription(t('The target entity.'))
      ->setComputed(TRUE);

    $fields['score'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Score'))
      ->setDescription(t('Some types may need a score to be set'))
      ->setSettings([
        'unsigned', TRUE,
        'min' => 0,
        'max' => 100,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    return $fields;
  }

}
